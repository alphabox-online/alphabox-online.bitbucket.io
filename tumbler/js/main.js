function pianoPlayed(e) {
	var prevTarget = pianoTarget;
	var prevStyle = pianoStyle;
	var noteDrop = document.getElementById('noteNameSelect');
	var octaveDrop = document.getElementById('noteOctaveSelect');
	
	// ul itself can be clicked, which is not desired
	if ( e.target.localName != "li" )
		return;
	
	if ( e.target != prevTarget ) {
		pianoTarget = e.target;
		pianoStyle = e.target.style.background;
	}
	
	if ( prevTarget != null ) {
		prevTarget.style.setProperty("background", prevStyle);
	}
	
	noteDrop.value = pianoTarget.value;
	
	pianoTarget.style.setProperty("background", "linear-gradient(to bottom, white, rgba(255,220,30))");
	
	const now = Tone.now();
	synth.set({"envelope": {"release": decay.value/2 }});
	synth.triggerAttackRelease(noteNames[noteDrop.value] + octaveDrop.value.toString(), 0.1, now);
	
}
function ballButtonClick() {
	var ballButton = document.getElementById('ballButton');
	var wallButton = document.getElementById('wallButton');
	var addButton = document.getElementById('addNoteButton');
	var resetButton = document.getElementById('resetNoteButton');
	ballButton.disabled = true;
	wallButton.disabled = false;
	addButton.disabled = true;
	resetButton.disabled = true;
	wallMode = false;
	
	// Give every ball a note from the walls
	for ( var i = 0; i < balls.length; i++ ) {
		balls[i].note = notes[i % notes.length];
		balls[i].octave = noteOctaves[i % notes.length];
	}
	
	// Pad out notes/walls array to instead handle proper coloration
	for ( var i = 0; i < noteHits.length; i++ ) {
		noteHits[i] = 0.0;
	}
	for ( var i = noteHits.length; i < 90; i++ ) {
		notes.push(0);
		noteOctaves.push(4);
		noteHits.push(0.0);
		noteHitColors.push(new colorContainer(255,255,255));
	}
}
function wallButtonClick() {
	var ballButton = document.getElementById('ballButton');
	var wallButton = document.getElementById('wallButton');
	var addButton = document.getElementById('addNoteButton');
	var resetButton = document.getElementById('resetNoteButton');
	ballButton.disabled = false;
	wallButton.disabled = true;
	addButton.disabled = false;
	resetButton.disabled = false;
	wallMode = true;
	
	randomNotes();
}
function drawButtonClick() {
  var drawButton = document.getElementById('drawButton');
  var eraseButton = document.getElementById('eraseButton');
  drawButton.disabled = true;
  eraseButton.disabled = false;
  eraseMode = false;
}
function eraseButtonClick() {
  var drawButton = document.getElementById('drawButton');
  var eraseButton = document.getElementById('eraseButton');
  drawButton.disabled = false;
  eraseButton.disabled = true;
  eraseMode = true;
}
function addNoteButtonClick() {
	var noteName = document.getElementById('noteNameSelect').value;
	var noteOctave = document.getElementById('noteOctaveSelect').value;
	
	if ( wallMode && notes.length < 30 ) {
		notes.push(noteName);
		noteOctaves.push(noteOctave);
		noteHitColors.push(new colorContainer(255,255,255));
		noteHits.push(1.0);
	}
}
function resetNoteButtonClick() {
	if ( wallMode ) {
	var noteName = document.getElementById('noteNameSelect').value;
	var noteOctave = document.getElementById('noteOctaveSelect').value;
	notes = [noteName];
	noteOctaves = [noteOctave];
	noteHitColors = [new colorContainer(255,255,255)];
	noteHits = [1.0];
	}
}
function shakeUp() {
	for(var i = 0; i < balls.length; i++){
		balls[i].velocity.x = -8 + Math.random()*16;
		balls[i].velocity.y = -8 + Math.random()*16;
		balls[i].position.x -= 8 - Math.random()*16;
		balls[i].position.y -= 8 - Math.random()*16;
	}
	shakeOffset = 1.0;
}
function randomNotes() {
	var oct = 4;
	if ( ( Math.random() * 4 ) < 1 )
		oct--;
	if ( ( Math.random() * 4 ) < 1 )
		oct++;
	
	var a = Math.floor(Math.random() * 12);
	var major = Math.random() < 0.7 ? 1 : 0;

	if ( wallMode ) {

		switch ( Math.floor(Math.random() * 5) ) {
			case 0:
				notes = [a, (a + 2) % 12, (a + 3 + major) % 12, (a + 5) % 12, (a + 7) % 12, (a + 8 + major) % 12, (a + 10 + major) % 12];
				noteOctaves = [oct, oct + Math.floor((a+2)/12), oct + Math.floor((a+3+major)/12), oct + Math.floor((a+5)/12), oct + Math.floor((a+7)/12), oct + Math.floor((a+8+major)/12), oct + Math.floor((a+10+major)/12)];
				if ( Math.random() < 0.4 ) {
					notes.splice(0, 0, a);
					noteOctaves.splice(0, 0, oct);
				}
			break;
			case 1:
				notes = [a, (a + 3 + major) % 12, (a + 7) % 12];
				noteOctaves = [oct, oct + Math.floor((a+3+major)/12), oct + Math.floor((a+7)/12)];
				if ( Math.random() < 0.4 ) {
					notes.splice(0, 0, a);
					noteOctaves.splice(0, 0, oct);
				}
				if ( Math.random() < 0.4 ) {
					notes.push((a + 7) % 12);
					noteOctaves.push(oct + Math.floor((a + 7)/12));
				}
			break;
			case 2:
				notes = [a, a, (a + 5 + major*2) % 12, (a + 5 + major*2) % 12, a, (a + 5 + major*2) % 12, a];
				noteOctaves = [oct, oct, oct + Math.floor((a+5+major*2)/12), oct + Math.floor((a+5+major*2)/12), oct-1, oct-1 + Math.floor((a+5+major*2)/12), oct-2];
				if ( Math.random() < 0.4 ) {
					notes.splice(0, 0, a);
					noteOctaves.splice(0, 0, oct);
				}
			break;
			case 3:
				notes = [a, (a + 2) % 12, (a + 3 + major) % 12, (a + 7) % 12];
				noteOctaves = [oct, oct + Math.floor((a+2)/12), oct + Math.floor((a+3+major)/12), oct + Math.floor((a+7)/12)];
				if ( Math.random() < 0.4 ) {
					notes.splice(0, 0, a);
					noteOctaves.splice(0, 0, oct);
				}
			break;
			case 4:
				notes = [a, (a + 3) % 12, (a + 5) % 12, (a + 6) % 12, (a + 7) % 12, (a + 10) % 12];
				noteOctaves = [oct, oct + Math.floor((a+3)/12), oct + Math.floor((a+5)/12), oct + Math.floor((a+6)/12), oct + Math.floor((a+7)/12), oct + Math.floor((a+10)/12)];
				if ( Math.random() < 0.4 ) {
					notes.splice(0, 0, a);
					noteOctaves.splice(0, 0, oct);
				}
			break;
		}
	
		noteHitColors = [];
		noteHits = [];
		for ( var i = 0; i < notes.length; i++ ) {
			noteHitColors.push(new colorContainer(255,255,255));
			noteHits.push(0);
		}
		tumbleAng = Math.PI * 2 * Math.random();
		
	} else {
		var scale = Math.floor( Math.random() * 12 );
		var scaleNotes = [];
		switch ( scale ) {
			case 0: // Free
				scaleNotes = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11];
			break;
			case 1: // Major
				scaleNotes = [0, 2, 4, 5, 7, 9, 11];
			break;
			case 2: // Minor
				scaleNotes = [0, 2, 3, 5, 7, 8, 10];
			break;
			case 3: // Mixolydian
				scaleNotes = [0, 2, 4, 5, 7, 9, 10];
			break;
			case 4: // Lydian
				scaleNotes = [0, 2, 4, 6, 7, 9, 11];
			break;
			case 5: // Dorian
				scaleNotes = [0, 2, 3, 5, 7, 9, 11];
			break;
			case 6: // Phrygian
				scaleNotes = [0, 1, 3, 5, 7, 8, 10];
			break;
			case 7: // Locrian
				scaleNotes = [0, 1, 3, 5, 6, 8, 10];
			break;
			case 8: // Blues
				scaleNotes = [0, 3, 5, 6, 7, 10];
			break;
			case 9: // Major Pentatonic
				scaleNotes = [0, 2, 4, 7, 9];
			break;
			case 10: // Minor Pentatonic
				scaleNotes = [0, 3, 5, 7, 10];
			break;
			case 11: // Whole Tone
				scaleNotes = [0, 2, 4, 6, 8, 10];
			break;
		}
		
		// Add more tonic notes to establish it
		for( var i = 0; i < balls.length; i += 5 )
			scaleNotes.unshift(0);
		
		// Room for more octaves
		if ( balls.length > 7 )
			oct--;
		
		var j = 0;
		for ( var i = 0; i < balls.length; i++ ) {
			balls[i].note = (a + scaleNotes[j % scaleNotes.length]) % 12;
			balls[i].octave = oct + Math.floor((a + scaleNotes[j % scaleNotes.length])/12 + j/scaleNotes.length);
			j++;
			if ( Math.random() < 0.3 ) j++; // Sometimes, skip a scale degree.
		}
	}
}
function Ball(x, y, radius, e, mass, r, g, b){
	this.position = {x: x, y: y}; //m
	this.velocity = {x: 0, y: 0}; // m/s
	this.e = -e; // has no units
	this.mass = mass; //kg
	this.radius = radius; //m
	this.color = new colorContainer(r, g, b);
	this.r = r; this.g = g; this.b = b;
	this.justHit = 0;
	this.note = 0;
	this.octave = 4;
	this.area = (Math.PI * radius * radius) / 10000; //m^2
}
function colorContainer(r, g, b) {
  this.str = "rgb(" + r + "," + g + "," + b + ")";
  this.r = r;
  this.g = g;
  this.b = b;
}

function oscTypeChanged() {
	var osc = document.getElementById('oscTypeSelect').value;
	synth.set({"oscillator": {"type": osc}});
}

//colorChannelA and colorChannelB are ints ranging from 0 to 255
function colorChannelMixer(colorChannelA, colorChannelB, amountToMix){
    var channelA = colorChannelA*amountToMix;
    var channelB = colorChannelB*(1-amountToMix);
    return parseInt(channelA+channelB);
}
//rgbA and rgbB are arrays, amountToMix ranges from 0.0 to 1.0
//example (red): rgbA = [255,0,0]
function colorMixer(rgbA, rgbB, amountToMix){
    var r = colorChannelMixer(rgbA[0],rgbB[0],amountToMix);
    var g = colorChannelMixer(rgbA[1],rgbB[1],amountToMix);
    var b = colorChannelMixer(rgbA[2],rgbB[2],amountToMix);
    return "rgb("+r+","+g+","+b+")";
}
const synth = new Tone.PolySynth().toMaster();
var canvas = null;
var ctx = null;
var fps = 1/60; //60 FPS
var dt = fps * 1000; //ms 
var timer = false;
var Cd = 0.47;
var rho = 1.22; //kg/m^3 
var mouse = {x: 0, y:0, isDown: false};
var ag = 9.81; //m/s^2 acceleration due to gravity on earth = 9.81 m/s^2. 
var width = 0;
var height = 0;
var shakeOffset = 0;
var balls = [];
var notes = [0, 0, 0, 4, 7];
var noteOctaves = [3, 4, 4, 4, 4];
var noteNames = ["C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"];
var noteHits = [0, 0, 0, 0, 0];
var noteHitColors = [new colorContainer(255,255,255), new colorContainer(255,255,255), new colorContainer(255,255,255), new colorContainer(255,255,255), new colorContainer(255,255,255)];
var tumbleAng = 0;
var eraseMode = false;
var wallMode = true;
var hidePointerPreview = false;
var pianoTarget = null;
var pianoStyle = "";

function setup(){
	canvas = document.getElementById('canvas');
	ctx = canvas.getContext('2d');
	width = canvas.width;
	height = canvas.height;
	Tone.Master.volume.value = -10; //I get some crackling at default volume on my computer. Not sure why
	oscTypeChanged();
	canvas.onmousedown = mouseDown;
	canvas.onmouseup = mouseUp;
	canvas.onmousemove = getMousePosition;
	canvas.onmouseleave = hidePointerPreview;
	timer = setInterval(loop, dt);
}


var hidePointerPreview = function(e) {
  hidePointerPreview = true;
}

var mouseDown = function(e){
	if(e.which == 1){
		getMousePosition(e);
		mouse.isDown = true;
		if ( eraseMode == false ) {
		  var helperText = document.getElementById('helperText');
		  helperText.classList.remove("glow");
		  var max = 255;
		  var min = 20;
		  var r = 75 + Math.floor(Math.random() * (max - min) - min);
		  var g = 75 + Math.floor(Math.random() * (max - min) - min);
		  var b = 75 + Math.floor(Math.random() * (max - min) - min);
		  balls.push(new Ball(mouse.x, mouse.y, 10, 1, 1, r, g, b));
		  if ( !wallMode ) {
			  var noteName = document.getElementById('noteNameSelect').value;
			  var noteOctave = document.getElementById('noteOctaveSelect').value;
			  balls[balls.length-1].note = noteName;
			  balls[balls.length-1].octave = noteOctave;
		  }
		}
	}
}

var mouseUp = function(e){
	if(e.which == 1){
		mouse.isDown = false;
		if ( eraseMode == false ) {
		  balls[balls.length - 1].velocity.x = (balls[balls.length - 1].position.x - mouse.x) / 10;
		  balls[balls.length - 1].velocity.y = (balls[balls.length - 1].position.y - mouse.y) / 10;
		}
	}
}

function getMousePosition(e){
	hidePointerPreview = false;
	mouse.x = e.pageX - canvas.offsetLeft;
	mouse.y = e.pageY - canvas.offsetTop;
}

function loop(){
	//create constants
	var gravity = document.getElementById("gravity");
	var density = 0;
	var drag = 0;
	var decay = document.getElementById("decay");
	var tumbleSpd = document.getElementById("tumblespeed");
	var simSpd = document.getElementById("simspeed").value;
  
  tumbleAng = ( tumbleAng + tumbleSpd.value/10 ) % ( Math.PI * 2 );
  
  if ( shakeOffset > 0 )
	shakeOffset -= 0.08;
  
  // Note hits heal
  for (var i = 0; i < notes.length; i++) {
    if ( noteHits[i] > 0 ) {
      noteHits[i] = Math.max( 0.0, noteHits[i] - 0.035 );
    }
  }
  
	//Clear window at the begining of every frame
  ctx.fillStyle = "#14061D";
  ctx.fillRect(0, 0, width, height);
  
  if ( eraseMode && !hidePointerPreview ) {
    ctx.beginPath();
    if ( mouse.isDown ) {
      ctx.fillStyle = "rgb(255, 255, 0)";
      
	  // Remove balls within mouse cursor eraser radius
      for ( var i = 0; i < balls.length; i++ ) {
        var xd = balls[i].position.x - mouse.x;
        var yd = balls[i].position.y - mouse.y;
        if ( Math.sqrt( xd * xd + yd * yd ) < 43 ) {
          balls.splice(i, 1);
          i--;
        }
      }
    }
    else {
      ctx.fillStyle = "rgb(125, 125, 10)";
    }
    ctx.arc(mouse.x, mouse.y, 40, 0, 2 * Math.PI, true);
    ctx.fill();
    ctx.closePath();
  }
  
  for (var i = 0; i < notes.length; i++ ) {
    var start = i;
    while ( i < notes.length-1 && notes[i+1] == notes[start] && noteOctaves[i+1] == noteOctaves[start] && wallMode ) {
      i++;
    }
    var startAng = tumbleAng + ( start / notes.length ) * 2 * Math.PI + 0.02 * wallMode;
    var endAng = tumbleAng + ( (i + 1) / notes.length ) * 2 * Math.PI - 0.02 * wallMode;
	var sox = shakeOffset * ( -6 + 12 * Math.random() );
	var soy = shakeOffset * ( -6 + 12 * Math.random() );
    ctx.lineWidth = 2;
    ctx.strokeStyle = colorMixer([noteHitColors[i].r, noteHitColors[i].g, noteHitColors[i].b], [255, 255, 255], noteHits[i] * 2);
    ctx.fillStyle = colorMixer([noteHitColors[i].r, noteHitColors[i].g, noteHitColors[i].b], [20, 6, 29], noteHits[i] / 4);
    ctx.beginPath();
    ctx.arc(sox + width/2, soy + height/2, 200, startAng, endAng);
    ctx.arc(sox + width/2, soy + height/2, 250, endAng + 0.005, startAng - 0.005, 1);
	ctx.arc(sox + width/2, soy + height/2, 200, startAng, startAng);
    ctx.fill();
	
	if ( wallMode ) {
		ctx.beginPath();
		ctx.arc(sox + width/2, soy + height/2, 200, startAng, endAng);
		ctx.arc(sox + width/2, soy + height/2, 250, endAng + 0.005, startAng - 0.005, 1);
		ctx.arc(sox + width/2, soy + height/2, 200, startAng, startAng);
		ctx.stroke();
	    
		ctx.fillStyle = "white";
		ctx.font = "32px sans-serif";
		ctx.textAlign = "center";
		ctx.textBaseline = "middle";
		var medAng = (endAng + startAng)/2;
		ctx.fillText(noteNames[notes[start]], sox + width/2 - 3 + Math.cos(medAng) * 225, soy + height/2 + Math.sin(medAng) * 225);
		ctx.font = "14px sans-serif";
		ctx.fillText(noteOctaves[start], sox + width/2 + 15 + Math.cos(medAng) * 225, soy + height/2 + 10 + Math.sin(medAng) * 225);
	}
  }
  
  if ( !wallMode ) {
	ctx.strokeStyle = "white";
	ctx.beginPath();
	ctx.arc(sox + width/2, soy + height/2, 200, 0, Math.PI * 2 );
	ctx.stroke();
	ctx.beginPath();
	ctx.arc(sox + width/2, soy + height/2, 250, 0, Math.PI * 2 );
	ctx.stroke();
  }

	for(var i = 0; i < balls.length; i++){
		if(!mouse.isDown || eraseMode || i != balls.length - 1){
			//physics - calculating the aerodynamic forces to drag
			// -0.5 * Cd * A * v^2 * rho
			var fx = -0.5 * drag * density * balls[i].area * balls[i].velocity.x * balls[i].velocity.x * (balls[i].velocity.x / Math.abs(balls[i].velocity.x));
			var fy = -0.5 * drag * density * balls[i].area * balls[i].velocity.y * balls[i].velocity.y * (balls[i].velocity.y / Math.abs(balls[i].velocity.y));

			fx = (isNaN(fx)? 0 : fx);
			fy = (isNaN(fy)? 0 : fy);
			console.log(fx);
			//Calculating the accleration of the ball
			//F = ma or a = F/m
			var ax = fx / balls[i].mass;
			var ay = (ag * gravity.value) + (fy / balls[i].mass);

			//Calculating the ball velocity 
			balls[i].velocity.x += ax * 0.01 * simSpd;
			balls[i].velocity.y += ay * 0.01 * simSpd;

			//Calculating the position of the ball
			balls[i].position.x += balls[i].velocity.x * simSpd;
			balls[i].position.y += balls[i].velocity.y * simSpd;
		}
		
		balls[i].justHit -= 0.2;
    
 		//Handling the ball collisions
		collisionBall(balls[i]);
		collisionWall(balls[i]);	
		
		//Rendering the ball
		ctx.beginPath();
		ctx.strokeStyle = balls[i].color.str;
		ctx.lineWidth = 3;
		ctx.arc(balls[i].position.x, balls[i].position.y, balls[i].radius, 0, 2 * Math.PI, true);
		ctx.stroke();
		ctx.closePath();
		
		// Note name over top of ball
		if ( !wallMode ) {
			ctx.fillStyle = "white";
			ctx.font = "12px sans-serif";
			ctx.textAlign = "center";
			ctx.textBaseline = "middle";
			ctx.fillText(noteNames[balls[i].note], balls[i].position.x, balls[i].position.y);
			
		}
  
	}
	
	if(mouse.isDown && eraseMode == false){
		ctx.beginPath();
		ctx.strokeStyle = "white";
		ctx.moveTo(balls[balls.length - 1].position.x, balls[balls.length - 1].position.y);
		ctx.lineTo(mouse.x, mouse.y);
		ctx.stroke();
		ctx.closePath();
	}

}
	
function collisionWall(ball){
	if(ball.position.x > width - ball.radius){
		ball.velocity.x *= ball.e;
		ball.position.x = width - ball.radius;
	}
	if(ball.position.y > height - ball.radius){
		ball.velocity.y *= ball.e;
		ball.position.y = height - ball.radius;
	}
	if(ball.position.x < ball.radius){
		ball.velocity.x *= ball.e;
		ball.position.x = ball.radius;
	}
	if(ball.position.y < ball.radius){
		ball.velocity.y *= ball.e;
		ball.position.y = ball.radius;
	}
  var xc = ball.position.x - width/2;
  var yc = ball.position.y - height/2;
  var dist = Math.sqrt( xc * xc + yc * yc );
  if(dist > 200 - ball.radius){
    var normalX = xc / dist;
    var normalY = yc / dist;
    var tangentX = -normalY;
    var tangentY = normalX;
    var normalSpeed = -(normalX * ball.velocity.x + normalY * ball.velocity.y);
    var tangentSpeed = tangentX * ball.velocity.x + tangentY * ball.velocity.y;
    ball.position.x = width/2 + xc / dist * (200 - ball.radius);
    ball.position.y = height/2 + yc / dist * (200 - ball.radius);
    ball.velocity.x = normalSpeed * normalX + tangentSpeed * tangentX;
    ball.velocity.y = normalSpeed * normalY + tangentSpeed * tangentY;
    
    // Calc angle of collision for hit calc
	  var ang = Math.atan2(normalY, normalX);
	  while ( ang < tumbleAng || ang < 0 )
		ang += Math.PI * 2;
	  for ( var i = 0; i < notes.length; i++ ) {
		while ( i < notes.length - 1 && notes[i+1] == notes[i] && noteOctaves[i+1] == noteOctaves[i] && wallMode ) {
		  i++;
		}
		if ( ang <= tumbleAng + ( ( i + 1 ) / notes.length ) * Math.PI * 2 ) {
		  if ( noteHits[i] < 0.97 && ball.justHit <= 0 ) {
			noteHits[i] = 1.0;
			
			noteHitColors[i] = new colorContainer(ball.color.r, ball.color.g, ball.color.b);
			
			const now = Tone.now();
			synth.set({"envelope": {"release": decay.value/2 }});
			
			// Also affect nearby walls in ball mode
			if ( !wallMode ) {
				noteHits[(i+1)%noteHits.length] = 0.95;
				noteHits[(i+2)%noteHits.length] = 0.9;
				noteHits[(i+noteHits.length-1)%noteHits.length] = 0.95;
				noteHits[(i+noteHits.length-2)%noteHits.length] = 0.9;
				noteHitColors[(i+1)%noteHits.length] = new colorContainer(ball.color.r*0.85, ball.color.g*0.85, ball.color.b*0.85);
				noteHitColors[(i+2)%noteHits.length] = new colorContainer(ball.color.r*0.55, ball.color.g*0.55, ball.color.b*0.55);
				noteHitColors[(i+noteHits.length-1)%noteHits.length] = new colorContainer(ball.color.r*0.85, ball.color.g*0.85, ball.color.b*0.85);
				noteHitColors[(i+noteHits.length-2)%noteHits.length] = new colorContainer(ball.color.r*0.55, ball.color.g*0.55, ball.color.b*0.55);
				synth.triggerAttackRelease(noteNames[ball.note] + ball.octave.toString(), 0.1, now);
			}
			else {	
				synth.triggerAttackRelease(noteNames[notes[i]] + noteOctaves[i].toString(), 0.1, now);
			}
		  }
		  ball.justHit = 1.0;
		  i = notes.length;
		}
	  }
  }
}
function collisionBall(b1){
	for(var i = 0; i < balls.length; i++){
		var b2 = balls[i];
		if(b1.position.x != b2.position.x && b1.position.y != b2.position.y){
			//quick check for potential collisions using AABBs
			if(b1.position.x + b1.radius + b2.radius > b2.position.x
				&& b1.position.x < b2.position.x + b1.radius + b2.radius
				&& b1.position.y + b1.radius + b2.radius > b2.position.y
				&& b1.position.y < b2.position.y + b1.radius + b2.radius){
				
				//pythagoras 
				var distX = b1.position.x - b2.position.x;
				var distY = b1.position.y - b2.position.y;
				var d = Math.sqrt((distX) * (distX) + (distY) * (distY));
	
				//checking circle vs circle collision 
				if(d < b1.radius + b2.radius){
					var nx = (b2.position.x - b1.position.x) / d;
					var ny = (b2.position.y - b1.position.y) / d;
					var p = 2 * (b1.velocity.x * nx + b1.velocity.y * ny - b2.velocity.x * nx - b2.velocity.y * ny) / (b1.mass + b2.mass);

					// calulating the point of collision 
					var colPointX = ((b1.position.x * b2.radius) + (b2.position.x * b1.radius)) / (b1.radius + b2.radius);
					var colPointY = ((b1.position.y * b2.radius) + (b2.position.y * b1.radius)) / (b1.radius + b2.radius);
					
					//stoping overlap 
					b1.position.x = colPointX + b1.radius * (b1.position.x - b2.position.x) / d;
					b1.position.y = colPointY + b1.radius * (b1.position.y - b2.position.y) / d;
					b2.position.x = colPointX + b2.radius * (b2.position.x - b1.position.x) / d;
					b2.position.y = colPointY + b2.radius * (b2.position.y - b1.position.y) / d;

					//updating velocity to reflect collision 
					b1.velocity.x -= p * b1.mass * nx;
					b1.velocity.y -= p * b1.mass * ny;
					b2.velocity.x += p * b2.mass * nx;
					b2.velocity.y += p * b2.mass * ny;
				}
			}
		}
	}
}